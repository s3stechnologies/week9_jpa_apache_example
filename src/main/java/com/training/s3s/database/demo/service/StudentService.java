package com.training.s3s.database.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.training.s3s.database.demo.model.Student;
import com.training.s3s.database.demo.repo.StudentRepo;

@Service
public class StudentService {

	@Autowired
	private StudentRepo repo;

	public List<Student> getListOfStudents() {
		return repo.findAll();
	}

	public void postStudent(List<Student> student) {

		repo.saveAll(student);

	}

	public Optional<Student> getStudentByid(int id) {

		return repo.findById(id);

	}

	public void updateStudent(Student student) {

		repo.save(student);

	}

	public void deleteStudentByid(int id) {

		repo.deleteById(id);

	}

}
