package com.training.s3s.database.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApacheDerbyDataBaseDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApacheDerbyDataBaseDemoApplication.class, args);
	}

}
