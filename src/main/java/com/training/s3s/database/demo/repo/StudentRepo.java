package com.training.s3s.database.demo.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.training.s3s.database.demo.model.Student;



@Repository
public interface StudentRepo extends JpaRepository<Student, Integer> {

	
	
	
}
